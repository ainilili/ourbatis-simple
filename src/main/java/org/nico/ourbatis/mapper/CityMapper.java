package org.nico.ourbatis.mapper;

import org.nico.ourbatis.domain.City;

public interface CityMapper extends BaseMapper<City, String>{

}
